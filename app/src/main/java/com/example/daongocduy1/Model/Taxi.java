package com.example.daongocduy1.Model;

import java.io.Serializable;

public class Taxi implements Serializable {
    private int Ma;
    private String carNumber;
    private float distance;
    private int pricing;
    private int discount;

    public Taxi(String carNumber, float distance, int pricing, int discount) {
        this.carNumber = carNumber;
        this.distance = distance;
        this.pricing = pricing;
        this.discount = discount;
    }

    public Taxi(int id, String carNumber, float distance, int pricing, int discount) {
        this.Ma = id;
        this.carNumber = carNumber;
        this.distance = distance;
        this.pricing = pricing;
        this.discount = discount;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getPricing() {
        return pricing;
    }

    public void setPricing(int pricing) {
        this.pricing = pricing;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getMa() {
        return Ma;
    }

    public void setMa(int ma) {
        Ma = ma;
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "Ma=" + Ma +
                ", carNumber='" + carNumber + '\'' +
                ", distance=" + distance +
                ", pricing=" + pricing +
                ", discount=" + discount +
                '}';
    }
}
