package com.example.daongocduy1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.daongocduy1.Activity.EditActivity;
import com.example.daongocduy1.Adapter.TaxiAdapter;
import com.example.daongocduy1.Helper.DBHelper;
import com.example.daongocduy1.Model.Taxi;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    ArrayList<Taxi> listTaxi;
    TaxiAdapter taxiAdapter;
    DBHelper db;
    ListView lvTaxi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = DBHelper.getInstance(this);
        setContentView(R.layout.activity_main);
        lvTaxi = findViewById(R.id.lv_items);
        registerForContextMenu(lvTaxi);
        db.recreateTable();
        createData();
        listTaxi = db.getAll();
        taxiAdapter = new TaxiAdapter(this, R.layout.taxi_item, listTaxi);
        lvTaxi.setAdapter(taxiAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listTaxi = db.getAll();
        taxiAdapter.clear();
        taxiAdapter.addAll(db.getAll());
        taxiAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, Menu.FIRST, 0, getText(R.string.edit));
        menu.add(1, Menu.FIRST + 1, 1, getText(R.string.delete));
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    private void createData() {
        db.insert(new Taxi("17k6", 12, 20000, 5));
        db.insert(new Taxi("23k6", 12, 20000, 5));
        db.insert(new Taxi("14k6", 12, 20000, 5));
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = adapterContextMenuInfo.position;
        if (item.getItemId() == 2) {
            Taxi taxi = listTaxi.get(position);
            db.delete(taxi.getMa());
            listTaxi = db.getAll();
            taxiAdapter.clear();
            taxiAdapter.addAll(db.getAll());
            taxiAdapter.notifyDataSetChanged();
        } else if (item.getItemId() == 1) {
            Taxi taxi = listTaxi.get(position);
            Bundle bundle = new Bundle();
            bundle.putSerializable("taxi", taxi);
            startActivity(new Intent(MainActivity.this, EditActivity.class).putExtras(bundle));
        }
        return super.onContextItemSelected(item);
    }
}