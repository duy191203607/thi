package com.example.daongocduy1.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.daongocduy1.Model.Taxi;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper sInstance;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "SqliteDB_09112001";
    private static final String TABLE_NAME = "Taxi_1";
    private static final String ID_COLUMN = "Ma";
    private static final String CAR_NUMBER = "So_Xe";
    private static final String DISTANCE = "Quang_Duong";
    private static final String PRICING = "Don_Gia";
    private static final String DISCOUNT = "Khuyen_Mai";
    private final String TAG = "DBLogger";
    private static final String CREATE_WORD_TABLE_SQL =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    CAR_NUMBER + " TEXT NOT NULL," +
                    DISTANCE + " FLOAT NOT NULL," +
                    PRICING + " INTEGER NOT NULL," +
                    DISCOUNT + " INTEGER NOT NULL" +
                    ")";

    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "onCreate: ");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "onUpgrade: ");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public static DBHelper getInstance(Context context) {
        if (sInstance == null)
            sInstance = new DBHelper(context.getApplicationContext());
        return sInstance;
    }

    public ArrayList<Taxi> getAll() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<Taxi> taxis = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + CAR_NUMBER + " ASC";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                taxis.add(new Taxi(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getFloat(2),
                        cursor.getInt(3),
                        cursor.getInt(4)
                ));
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return taxis;
    }

    public void insert(Taxi taxi) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CAR_NUMBER, taxi.getCarNumber());
        values.put(DISTANCE, taxi.getDistance());
        values.put(PRICING, taxi.getPricing());
        values.put(DISCOUNT, taxi.getDiscount());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void update(Taxi taxi) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CAR_NUMBER, taxi.getCarNumber());
        values.put(DISTANCE, taxi.getDistance());
        values.put(PRICING, taxi.getPricing());
        values.put(DISCOUNT, taxi.getDiscount());
        db.update(TABLE_NAME, values, ID_COLUMN + " = ?", new String[]{String.valueOf(taxi.getMa())});
        db.close();
    }

    public void recreateTable() {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(sql);
        db.execSQL(CREATE_WORD_TABLE_SQL);
    }

    public void delete(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, ID_COLUMN + " = ?", new String[]{String.valueOf(id)});
        System.out.println("================" + getAll().toString());
        db.close();
    }
}
