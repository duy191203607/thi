package com.example.daongocduy1.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.daongocduy1.Model.Taxi;
import com.example.daongocduy1.R;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("SetTextI18n")

public class TaxiAdapter extends ArrayAdapter<Taxi> {
    private final Context mContext;
    private final List<Taxi> taxiArrayList;
    private final int resource;

    public TaxiAdapter(Context mContext, int resource, ArrayList<Taxi> taxiArrayList) {
        super(mContext, 0, taxiArrayList);
        this.mContext = mContext;
        this.taxiArrayList = taxiArrayList;
        this.resource = resource;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(resource, parent, false);
        Taxi taxi = taxiArrayList.get(position);
        TextView name = convertView.findViewById(R.id.taxiName);
        name.setText(taxi.getCarNumber());
        TextView distance = convertView.findViewById(R.id.distance);
        distance.setText("Quãng đường: " + taxi.getDistance() + " km");
        TextView pricing = convertView.findViewById(R.id.pricing);
        float totalPricing = taxi.getPricing() * taxi.getDistance() * (100 - taxi.getDiscount()) / 100;
        pricing.setText(String.valueOf(totalPricing));
        return convertView;
    }
}
