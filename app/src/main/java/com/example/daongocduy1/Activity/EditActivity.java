package com.example.daongocduy1.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.daongocduy1.Adapter.TaxiAdapter;
import com.example.daongocduy1.Helper.DBHelper;
import com.example.daongocduy1.Model.Taxi;
import com.example.daongocduy1.R;

public class EditActivity extends AppCompatActivity {
    EditText edtCarNumber, edtDistance, edtPricing, edtDiscount;
    DBHelper db;
    Taxi taxi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        db = DBHelper.getInstance(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        taxi = (Taxi) bundle.getSerializable("taxi");
        initDataFromIntent(taxi);
        addEvent();
    }

    private void initDataFromIntent(Taxi taxi) {
        edtCarNumber = findViewById(R.id.edtCarNumber);
        edtDistance = findViewById(R.id.edtDistance);
        edtPricing = findViewById(R.id.edtPricing);
        edtDiscount = findViewById(R.id.edtDiscount);
        edtCarNumber.setText(taxi.getCarNumber());
        edtDistance.setText(String.valueOf(taxi.getDistance()));
        float totalPricing = taxi.getPricing() * taxi.getDistance() * (100 - taxi.getDiscount()) / 100;
        edtPricing.setText(String.valueOf(totalPricing));
        edtDiscount.setText(String.valueOf(taxi.getDiscount()));
    }

    private void addEvent() {
        Button update = findViewById(R.id.btnEdit);
        Button delete = findViewById(R.id.btnDelete);
        update.setOnClickListener(v -> {
            Taxi taxiUpdate = new Taxi(taxi.getMa(),
                    edtCarNumber.getText().toString(),
                    Float.parseFloat(edtDistance.getText().toString()),
                    taxi.getPricing(),
                    Integer.parseInt(edtDiscount.getText().toString())
            );
            db.update(taxiUpdate);
            onBackPressed();
        });
        delete.setOnClickListener(v -> {
            onBackPressed();
        });
    }
}
